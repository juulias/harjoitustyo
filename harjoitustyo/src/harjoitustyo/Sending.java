/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author juulia
 */

// Class for the actions of sending the package
public class Sending {
    
    private ArrayList<HashMap> history;
    private History H;
    
    public Sending(History h) {
        H = h;
        history = new ArrayList();
  
    }
    
    
    public String send(SmartPost p1, SmartPost p2, Package p) throws IOException {
            ArrayList<Float> route = new ArrayList();
            route.add(p1.getX());
            route.add(p1.getY());
            route.add(p2.getX());
            route.add(p2.getY());
            
            // Check if the object breaks
            if (objectBreak(p.breakPos, p.canBreak)) {
                p.breakObject();
            }
            
            
            Date d = new Date();
            SimpleDateFormat dd = new SimpleDateFormat("hh:mm");
            
            String intact;
            if (p.intact) {
                 intact = "Esine säilyi ehjänä!";
            } else {
                 intact = "Esine hajosi matkalla!";
            }

            // add information about the action into ArrayList<HashMap>, so that
            // it can be viewed later
            HashMap h = new HashMap();
            h.put("package", p.getName());
            h.put("class", p.getClassN());
            h.put("intact", intact);
            h.put("from", p1.getAddress());
            h.put("to", p2.getAddress());
            h.put("time", dd.format(d));
            
            history.add(h);
            H.saveCompleteLog(p1, p2, p);
            
            // return the ready-made javascript-function
            return "document.createPath(" + route +", 'red', "+ p.getClassN() +")";
    }
    
    public ArrayList<HashMap> getHistory() {
        return history;
    }
    
    // method to test, if the item breaks during the transportation
    private boolean objectBreak(int breakPos, boolean canBreak) {
        
        // if the item can break, lets use a random number to check if the
        // item actually does break. If the breaking possibility of the package
        // type is the highest, then the package breaks automatically, otherwise
        // the package is taken into account when checking if the item breaks.
        if (canBreak) {
            if (breakPos == 2) {
                return true;
            }
            else {
                int r = ThreadLocalRandom.current().nextInt(1, 10);
                if ((r * (breakPos + 1)) <10) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }
    
    
    // save a log of the current sessions actions
    public void saveLog(ArrayList<Package> storage) throws IOException {
        Date d = new Date();
        SimpleDateFormat dd = new SimpleDateFormat("dd.MM.yyyy");
        String log = "TIMO logikirjanpito " + dd.format(d)
                + "\n________________________\n\n" ;
        
        // go through all of the actions that have happened during the session
        for (HashMap h : history){
            log += "Tuote: " + h.get("package") + ", luokka: " + h.get("class") + "\n";
            log += "Lähtöautomaatti: " + h.get("from") + "\nKohdeautomaatti: " + h.get("to") + "\n";
            log += h.get("intact") + "\n\n";
        }
  
        log += "________________________\nVARASTOSSA:\n";
        for (Package p : storage) {
            log += "- " + p.getName() + ", luokka " + Integer.toString(p.getClassN()) + "\n";
        }
        String filename = "logi.txt";
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(log);
        out.close();
    }
}