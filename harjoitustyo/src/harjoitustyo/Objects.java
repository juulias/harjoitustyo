/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

/**
 *
 * @author ville
 */
public class Objects {
    protected String name;
    protected int width;
    protected int height;
    protected int depth;
    protected int weight;
    protected boolean canBreak;
    
    public Objects(String n, int w, int h, int d, int we, boolean b) {
        name = n;
        width = w;
        height = h;
        depth = d;
        weight = we;
        canBreak = b;

    }
    
    // methods returning Objects's attributes
    public String getName() {return name;}
    public int getWidth() {return width;}
    public int getHeight() {return height;}
    public int getDepth() {return depth;}
    public int getWeight() {return weight;}
    public boolean canBreak() {return canBreak;}

    @Override
    public String toString() {
        return name;
    }
}

class piano extends Objects {
    public piano () {
        super("piano", 100, 80, 29, 35, true);
    }
}

class brick extends Objects {
    public brick() {
        super("tiiliskivi, 5kpl", 25, 12, 25, 15, false);
    }
}

class jacket extends Objects {
    public jacket() {
        super("untuvatakki", 30, 15, 15, 10, false);
    }
}

class plate extends Objects {
    public plate() {
        super("antiikkilautanen", 20, 5, 20, 2, true);
    }
}