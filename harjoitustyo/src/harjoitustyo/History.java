/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juulia
 */

//Class for handling the historical data
public class History {
    
    public History() {
    }
    
    // returns a list of previous purchases in ready-to-use strings
    public ArrayList<String> listHistory(ArrayList<HashMap> a) {
        ArrayList<String> history = new ArrayList();
        for (HashMap h : a) {
            history.add("Aika " + h.get("time") + "\n" + h.get("package") + ", luokka " + h.get("class")
                    + "\nLähtö:" + h.get("from") + "\nKohde:" + h.get("to") +"\n" + h.get("intact"));
            }
        return history;
    }
    
    // The Log of everything that has been done with the software
    // writes actions one by one, is called right after some item has been sent.
    public void saveCompleteLog(SmartPost p1, SmartPost p2, Package p) throws IOException {
        Date d = new Date();
        SimpleDateFormat dd = new SimpleDateFormat("hh:mm dd.MM.yyyy");
        String log = dd.format(d) + ";" + p.getName() + ";"
                + p.getClassN() + ";" + p1.getAddress() + ";"
                + p2.getAddress() + ";" + p.intact + "\n";  

        String filename = "logiComplete.txt";
        
        BufferedWriter out = new BufferedWriter(new FileWriter(filename, true));
        out.write(log);
        out.close();

    }
    
    // Load the complete log of every sent item. Returns a ArrayList, which
    // can be used to display the information.
    public ArrayList<HashMap> loadCompleteLog(){
        String filename = "logiComplete.txt";
        ArrayList<HashMap> history = new ArrayList();
        
        try {
            BufferedReader in;
            in = new BufferedReader(new FileReader(filename));
            String inputLine;
            String intact;
            while ((inputLine = in.readLine()) != null) {
                
                // parse the information from the .txt
                String[] info = inputLine.split(";");
                String t = info[0];
                String n = info[1];
                int c = Integer.parseInt(info[2]);
                String a1 = info[3];
                String a2 = info[4];
                boolean b = Boolean.parseBoolean(info[5]);

                if (b) {
                 intact = "Esine säilyi ehjänä!";
                } else {
                    intact = "Esine hajosi matkalla!";
                }
                            
                HashMap h = new HashMap();
                h.put("package", n);
                h.put("class", c);
                h.put("intact", intact);
                h.put("from", a1);
                h.put("to", a2);
                h.put("time", t);
                history.add(h);
            }            
            in.close();

        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return history;
    }
}
