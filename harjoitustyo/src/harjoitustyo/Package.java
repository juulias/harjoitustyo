/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

/**
 *
 * @author ville
 */
abstract class Package {
    protected int classn;
    protected int width;
    protected int height;
    protected int depth;
    protected int weight;
    
    // the risks with this class:
    // rarely breaks (0), might break (1), breaks easily (2)
    protected int breakPos;
    protected Object obj;
    
    // can the item break
    protected boolean canBreak;
    protected boolean intact = true;
    
    public int getClassN() { return classn;}
    public int getWidth() { return width;}
    public int getHeight() { return height;}
    public int getDepth() { return depth;}
    public int getWeight() { return weight;}
    public boolean canBreak() { return canBreak;}
    public int getBreakPos() { return breakPos;}
    public String getName() {return obj.toString();}
    public boolean objectIntact() {return intact;}
    
    // if the object breakes, this method is used to change
    // its status
    public void breakObject() {
        intact = false;
    }
}


class FirstClass extends Package {
    
    public FirstClass(Objects o) {
        classn = 1;
        width = 50;
        height = 60;
        depth = 50;
        weight = 10;
        breakPos = 1;
        
        obj = o;
        canBreak = o.canBreak();
    }
    
}

class SecondClass extends Package {
    
    public SecondClass(Objects o) {
        classn = 2;
        width = 30;
        height = 30;
        depth = 30;
        weight = 2;
        breakPos = 0;
        
        obj = o;
        canBreak = o.canBreak();
    }
}


class ThirdClass extends Package {

        
    public ThirdClass(Objects o) {
        classn = 3;
        width = 100;
        height = 80;
        depth = 80;
        weight = 35;
        breakPos = 2;
        
        obj = o;
        canBreak = o.canBreak();
    }
}